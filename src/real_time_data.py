import os
import json
import requests
import sqlite3
import pandas as pd
import pycountry as cty
import plotly.express as px
import plotly.graph_objs as go
import country_converter as cc

database = sqlite3.connect('data/Database_covid.db') # connection to the data base Data_structur.text_factory = lambda b: b.decode(errors = 'ignore')
data_structur = database.cursor()
try:
    data_structur.execute('''CREATE TABLE COVID19_DATA ([Country] text,[Slug] text, [Newconfirmed] integer, [NewDeaths] integer, [TotalDeaths] integer, [NewRecovered] integer, [TotalRecovered] integer,])''')

except:
    pass

def real_time_dataÒ(url):
    # wait for refresh time can be used only if the program run with multiprocessing
    #time.sleep(360*refresh_time)
    genereal_data = requests.get(url).json()
    useful_data = genereal_data.get('Countries')
    with open('covid_19.json', 'w') as file:
        json.dump(useful_data, file)
        
url = "https://api.covid19api.com/summary" # this Url return the data as json file
data = real_time_data(url)
Dataframe = pd.read_json('covid_19.json')

Country_code = Dataframe.CountryCode.copy()
for cnts in range(len(Country_code)):
    try:
        Country_code[cnts] = cc.convert(names= Country_code[cnts], to='ISO3')
    except:
        pass
Dataframe.CountryCode = Country_code
Dataframe.to_sql('COVID19_DATA', database, if_exists='replace', index = False)
