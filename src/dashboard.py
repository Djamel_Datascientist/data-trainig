from datetime import timedelta
from pathlib import Path
from time import sleep
import geopandas 
import numpy as np
import pandas as pd
import plotly_express as px
import sqlite3
import streamlit as st
from chart_studio.plotly import plot, iplot
import plotly.graph_objs as go
from real_time_data_process import read_real_time_data
from PIL import Image
from sklearn.datasets import make_regression
import matplotlib.pyplot as plt
import seaborn as sns
#############################################################################
st.sidebar.markdown('** Chek information and projects **.')
page = st.sidebar.selectbox("Choose a page", ["Machine learning","COVID-19","Project","Contact"])


st.markdown("# :chart_with_upwards_trend: Data Science Project.")
st.markdown("  :bar_chart: On this web site you will find a lot of styled projects for data analysis and visualization  :wink: . ")

st.markdown(' @ MADE BY DJAMEL HAMIDI  :bar_chart: 🇩🇿 ')
st.markdown("Curent project :beginner: .")
# %%
@st.cache
def load_data():
    #read_real_time_data()
  
    bikes_data_path = Path() / 'covide_data.csv'
    data = pd.read_csv(bikes_data_path)
    return data

df = load_data()
df_preprocessed = df.copy()

if page == "COVID-19":

    st.markdown("## :bar_chart: COVID-19 pandemic data visualization. ")
    st.markdown("This application is a dashboard devloped for fun "
            "to explore the pandemic data with many data visualisation tools.")
    st.header("The databas table.")
    st.write("you can check all of the data on this table.")
    st.write(df)
    Geodataframe = geopandas.GeoDataFrame(
        df_preprocessed, geometry=geopandas.points_from_xy(df_preprocessed.Longitude, df_preprocessed.Latitude))

    limits = [(0,10),(100,500),(1500,10000),(10000,50000),(50000,1000000)]
    colors = ["royalblue","crimson","lightseagreen","orange","lightgrey"]
    cities = []
    scale = 500

    fig1 = go.Figure()

    for i in range(len(limits)):
        lim = limits[i]
        df_sub = Geodataframe[lim[0]:lim[1]]
        fig1.add_trace(go.Scattergeo(
            locationmode = 'country names',
            lon = df_sub['Longitude'],
            lat = df_sub['Latitude'],
            text = df_sub['Country']+ '<br>Affected ' + (df_sub['TotalConfirmed']).astype(str), 
            marker = dict(
                size = df_sub['TotalConfirmed']/scale,
                color = colors[i],
                line_color = 'blue',
                line_width=5,
                sizemode = 'area'
            ),
        ))
    fig1.update_geos(fitbounds="locations", visible=True)
    fig1.update_layout(margin={"r":0,"t":0,"l":0,"b":0})
    fig1.update_layout(
            title_text = 'Covid 19 Pandemic on the world'
            )

    Mapp2lot_chart = st.write(fig1)

    # %% Mapp
    fig2 = px.scatter_geo(df, locations="CountryCode", color="TotalDeaths", hover_name="Country", size='TotalConfirmed')
    fig2.update_geos(fitbounds="locations", visible=True)
    fig2.update_layout(margin={"r":0,"t":0,"l":0,"b":0})
    Mapplot_chart = st.write(fig2)

    st.subheader('Map 3')
    fig3 = px.scatter_mapbox(df_preprocessed, lat="Latitude", lon="Longitude",     color="TotalConfirmed", size="TotalDeaths",
                      color_continuous_scale=px.colors.cyclical.IceFire, zoom=2)
    fig3.update_layout(mapbox_style="open-street-map")
    fig3.update_layout(margin={"r":0,"t":0,"l":0,"b":0})

    Mapp3lot_chart = st.write(fig3)
    if st.checkbox('Show databas information'):
        st.subheader('Databas API : https://api.covid19api.com/summary ')
        st.markdown("** Description of the database **")
        st.write(df_preprocessed.describe())
    ###############################################################################
    st.markdown("**♟ Information ♟**")
    st.markdown("* This maps was build with python data manuipulation tools"
                " The data com from an API  and it's refreshed with a frequencey of 2hours"
                " after that database will be preprocessed and used for the graphics")
    ###############################################################################
    st.markdown("### About me :grin: ")
    st.markdown(" My name is Djamel Hamidi and I'm a python devloper working on machin learning sinc one year\n "
                " The data scince is the base of the machine leaning and i still learn every day\n"
                " My obejctif is to become a business inteligence analyst :chart_with_upwards_trend: :bar_chart: ")

    image = Image.open('djanet_way.jpg')
    st.image(image, caption='Somewhere on earth.',use_column_width=True)



elif page == "Machine learning":

    radio = st.sidebar.radio(label="", options=["Predictive modeling.", "Computer vision.", "Natural language processing ."," Natural language understanding."])
    st.title(radio)

    if radio == "Predictive modeling.":

        model_type = st.radio(label="Chose the exempel model to train:", options=["Linear model", "Nonlinear model"])
        if model_type == "Linear model":
            st.title("Linear regression model.")
            x, y = make_regression(n_samples=100, n_features=1, noise = 10)
            y = y + abs(y/2)

        else :
            st.title("Nonlinear regression model.")
            x, y = make_regression(n_samples=100, n_features=2, noise = 10)

        y = y.reshape(y.shape[0],1)
        st.markdown("## Data set :")
        st.markdown("### This is a random dataset generated to test a cost model prediction.")
        # %% 
        Data = pd.DataFrame(list(zip(x[:,0], y.flatten())), 
               columns =['x', 'y']) 

        fig7 = px.scatter(x=x[:,0],y=y[:,0])
        st.write(fig7)
        X = np.hstack((x, np.ones(x.shape)))
        X = np.hstack((x**2, X))
        ###############################################################################
        if model_type == "Nonlinear model":

            X = np.hstack((x, np.ones((x.shape[0], 1))))

        np.random.seed(0)
        theta = np.random.rand(3,1)
        ###############################################################################
        def model(X, theta):
            return X.dot(theta)
        ###############################################################################
        Model = model(X, theta)
        st.markdown(" * Cost function is the mean squar error, the mathematics expression is : \n  ## j(𝒂, 𝒃) =𝟏/𝟐𝒎∑( 𝒇(𝒙(𝒊)) − 𝒚(𝒊))²")
        st.markdown("  ### When we use this prediction model without training it geve this prediction : ")
        Data['Model'] = Model
        sns.scatterplot(data=Data)
        st.pyplot()
        st.write(Data.head())
        plt.scatter(x=x[:,0].flatten(), y=y)
        plt.scatter(x=x[:,0].flatten(), y=Model)
        st.pyplot()
        ###############################################################################
        def cost_function(X, y, theta):
            m = len(y)
            return 1/(2*m) * np.sum((model(X, theta) - y)**2)
        ###############################################################################
        st.markdown(" ")
        def grad(X, y, theta):
            m = len(y)
            return 1/m * X.T.dot(model(X, theta) - y)
        ###############################################################################
        def gradient_descent(X, y, theta, learning_rate, n_iteration):
            cost_history = np.zeros(n_iteration)
            for i in range(0, n_iteration):
                cost_history[i] = cost_function(X, y, theta)
                theta = theta - learning_rate * grad(X, y, theta)
            return theta, cost_history
        ###############################################################################
        st.markdown('** \nChose the training hyperparameters: **')
        learning_rate = st.number_input(label="learning rate", value=0.01, min_value=0., max_value=1., step=0.01)
        epoch= st.number_input(label="Epoch", value=100, min_value=100, max_value=1000, step=100)
        test_final, cost_history = gradient_descent(X, y, theta, learning_rate=learning_rate, n_iteration=epoch)
        ###############################################################################
        prediction = model(X, test_final)
        st.markdown('** Prediction of the trained model: **')
        plt.scatter(x[:,0], y)
        plt.scatter(x[:,0], prediction, c='r')
        st.pyplot()
        Data['Model'] = prediction
        sns.scatterplot(data=Data)
        st.pyplot()
        ###############################################################################
        def coef_determination(y, pred):
            u = ((y - pred)**2).sum()
            v = ((y - y.mean())**2).sum()
            return 1 - u/v
        st.write("The model accuracy :",coef_determination(y, prediction),"%")
        ###############################################################################
        st.markdown('** \nLearning rate curve: **')
        learning_data  = 1 - (cost_history / max(cost_history))
        st.line_chart(learning_data*100)
        ###############################################################################
    elif radio == "Computer vision.":  
        image2 = Image.open('Computer-Vision.png')
        st.image(image2, caption='Based on convolutional neural network.',use_column_width=True)


elif page == 'Contact':
    ###############################################################################
    st.markdown('** Contact: **')
    st.markdown('Email : djameleddinehamidi@gmail.com')
    st.markdown('Linkedin : https://www.linkedin.com/in/hamidi-djamel-eddine-389340153/ ')
    st.markdown('Fon number: +33688204191')
    ###############################################################################



